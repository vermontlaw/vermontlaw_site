<?php

/**
 * Local dev alias. To set these values from within this git repo:
 *
 * $ git config drush.root `git rev-parse --show-toplevel`/docroot
 * $ git config drush.uri 'vls.dev' # ...or whatever your local hostname is.
 */
exec('git config drush.root 2> /dev/null', $root);
exec('git config drush.uri 2> /dev/null', $uri);
if ($root && $uri) {
  $aliases['local'] = array(
    'root' => array_shift($root),
    'uri' => array_shift($uri),
    '#stage' => '@vls.stage',
    '#prod' => '@vls.prod',
  );
}

// Get the environments list from the dev server or a local memory cache.

// Check the cache first.
$environment_data = [];
if (function_exists('shmop_open')) {
  $shm_key = ftok(__FILE__, 't');
  $shmid = shmop_open($shm_key, 'c', 0755, 1024);
  $cached_environments_json = trim(shmop_read($shmid, 0, shmop_size($shmid)));
  $environment_data = json_decode($cached_environments_json);

  // The cached data is more than 5 minutes old, clear it so that we'll re-fetch.
  if (time() - $environment_data->timestamp > 300) {
    $environment_data = [];
  }
}

if (!$environment_data) {
  // Query the dev server for current branch environments.
  $environments_json = file_get_contents('http://colab:885KjuEYqBFt5LGETkNq@vtlaw.dev.colab.coop/');
  $environment_data = json_decode($environments_json);

  // Cache the environment list if we can.
  if (function_exists('shmop_open')) {
    shmop_write($shmid, $environments_json, 0);
  }
}

// Create an alias for each dev server branch environment.
foreach ($environment_data->environments as $environment) {
  $aliases[$environment] = array(
    'root' => '/opt/www/vtlaw/www/' . $environment . '/docroot',
    'uri' => $environment . '.vtlaw.dev.colab.coop',
    'remote-host' => 'vtlaw.dev.colab.coop',
    'remote-user' => 'vtlaw',
    'path-aliases' => array(
      '%drush-script' => '/opt/www/vtlaw/www/' . $environment . '/vendor/bin/drush',
    ),
  );
}

// Staging server alias.
$aliases['stage'] = array(
  'root' => '/opt/www/vtlaw/www/docroot',
  'uri' => 'vtlaw.stage.colab.coop',
  'remote-host' => 'vtlaw.stage.colab.coop',
  'remote-user' => 'vtlaw',
  'path-aliases' => array(
    '%drush-script' => '/opt/www/vtlaw/www/vendor/bin/drush',
  )
);

// Production server alias.
$aliases['prod'] = array(
  'root' => '/opt/www/vtlaw/www/docroot',
  'uri' => 'vtlaw.colab.coop',
  'remote-host' => '11.0.2.90',
  'remote-user' => 'vtlaw',
  'path-aliases' => array(
    '%drush-script' => '/opt/www/vtlaw/www/vendor/bin/drush',
  )
);
