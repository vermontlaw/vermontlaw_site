<?php
/**
 * @file
 * Custom drush configuration for Vermont Law School.
 */

$options['shell-aliases']['import-events'] = '!../vendor/bin/drush mi events;
  ../vendor/bin/drush php-eval "vtlaw_migrate_post_import_event()"';
$options['shell-aliases']['rollback-events'] = "!../vendor/bin/drush mr events;
  ../vendor/bin/drush sqlq 'DELETE FROM redirect WHERE redirect_source__path like \"news-and-events/event/%\"';
  ../vendor/bin/drush cr";
$options['shell-aliases']['import-file-assets'] = '!../vendor/bin/drush mi file_assets --feedback=100;
  ../vendor/bin/drush mi file_asset_media;
  ../vendor/bin/drush php-eval "vtlaw_migrate_post_import_file_assets()";
  ../vendor/bin/drush cr';
$options['shell-aliases']['rollback-file-assets'] = '!../vendor/bin/drush php-eval "vtlaw_migrate_pre_rollback_file_assets()";
  ../vendor/bin/drush mr file_asset_media;
  ../vendor/bin/drush mr file_assets
  ../vendor/bin/drush cr';
$options['shell-aliases']['import-news-releases'] = '!../vendor/bin/drush mi news_releases;
  ../vendor/bin/drush php-eval "vtlaw_migrate_post_import_news_release()"';
$options['shell-aliases']['rollback-news-releases'] = "!../vendor/bin/drush mr news_releases;
  ../vendor/bin/drush sqlq 'DELETE FROM redirect WHERE redirect_source__path like \"news-and-events/newsroom/press-release/%\"';
  ../vendor/bin/drush cr";
$options['shell-aliases']['import-pages'] = '!../vendor/bin/drush mi page_photos;
  ../vendor/bin/drush mi page_media;
  ../vendor/bin/drush mi page_iwcs;
  ../vendor/bin/drush mi page_inline_images;
  ../vendor/bin/drush mi page_inline_media;
  ../vendor/bin/drush mi sections;
  ../vendor/bin/drush mi pages;
  ../vendor/bin/drush php-eval "vtlaw_migrate_post_import_page()";
  ../vendor/bin/drush cr';
/*$options['shell-aliases']['rollback-pages'] = '!../vendor/bin/drush mr pages;
  ../vendor/bin/drush sqlq "DELETE FROM redirect WHERE 1";
  ../vendor/bin/drush cr';*/

$options['shell-aliases']['import-people'] = '!../vendor/bin/drush mi constituencies;
  ../vendor/bin/drush mi departments;
  ../vendor/bin/drush mi expertises;
  ../vendor/bin/drush mi people_photos;
  ../vendor/bin/drush mi people_media;
  ../vendor/bin/drush mi places;
  ../vendor/bin/drush mi people';

// Pull data from staging.
$options['shell-aliases']['sync-stage'] = '!drush sql-drop && drush sql-sync {{#stage}} {{@target}} && drush {{@target}} uli';

// Pull data from production.
$options['shell-aliases']['sync-prod'] = '!drush sql-drop && drush sql-sync {{#prod}} {{@target}} && drush {{@target}} uli';
