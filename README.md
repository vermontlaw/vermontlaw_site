# Vermont Law School Drupal Site

This is the main code repository for the Drupal 8 site for [Vermont Law School][]. It contains the custom site code, developer documentation, shared configuration files and scripts used for development and deployment.

## Project Documentation

* The Vermont Law [Confluence Space][] has links to *every* important
  document and resource in the project. Go here to review meeting
  notes, find tech details, or look for any needle in this project's
  haystack.
* The [Developer Documentation][] is the place to go to get
  details on the inner workings of the site, get set up to help
  contribute, or learn how to deploy code changes to the live site.
* Issue tracking is via [JIRA][].

## Team Facilitators

* [Margy](mailto:margy@colab.coop) is the main project manager
  for the project. In general, if you need anything talk with her.
* [Ethan](mailto:ethan@colab.coop) is the technical principle on the
  account. He's been involved in the project starting from the initial
  sales meeting and is here to help the tech team coordinate with the
  main goals of the client. He loves to talk.
* [Jeff](mailto:jeff@colab.coop) is the Drupal lead on the
  project. He's the one to brainstorm with about how to do this or
  that, and also is looking out for ways we can streamline development
  by making smart architecture choices.

[Vermont Law School]: http://www.vermontlaw.edu
[Confluence Space]: https://colabcoop.atlassian.net/wiki/pages/viewpage.action?spaceKey=VTLAW&title=VTLaw+Home
[Developer Documentation]: docs/index.md
[JIRA]: https://colabcoop.atlassian.net/secure/RapidBoard.jspa?rapidView=198&projectKey=VTLAW&view=planning.nodetail
