# Deployment

The development, staging, and production sites are hosted on Amazon infrastructure maintained by Colab.

The development site is located at http://vtlaw.dev.colab.coop/, and can host individual git branches as wildcard subdomains. For example:

- http://develop.vtlaw.dev.colab.coop/
- http://VTLAW-123.vtlaw.dev.colab.coop/

The staging site is located at http://vtlaw.stage.colab.coop/, and runs from the latest `release/*` branch.

Production is located at http://vtlaw.colab.coop/, and runs from the latest release tag.

## Deploying a branch to the dev server

Requirements:

- Your public ssh key on the `vtlaw` user at the host `vtlaw.dev.colab.coop`. If you can `ssh vtlaw@vtlaw.dev.colab.coop` you can deploy development branches.
- A one-time git alias setup, added by running `./scripts/setup-hooks.sh`.

You can then deploy your current git branch with the following command:

    $ git deploy

If you were on the `develop` branch, this would deploy a copy of the site to `http://develop.vtlaw.dev.colab.coop/`. If you were on a feature branch, for example `VTLAW-778`, your new site would be available at `http://VTLAW-778.vtlaw.dev.colab.coop/`.

If you want to deploy a branch other than the one currently checked out, you can specify it in the arguments:

    $ git deploy origin develop

`origin` is the name of the remote from which to deploy, which is BitBucket in our case.

All development sites are protected with HTTP authentication. See a team member for the credentials.

## Creating a Release

**Before deploying to staging or production**, you must create a new release. This project uses the [git flow][] process.

Requirements:

- Knowledge of [git flow][]
- [Git flow extensions][] (optional but highly recommended)

The steps below will assume use of the git flow extensions.

If you haven't already, run `git flow init` with the following settings:

```
Branch name for production releases: [master]
Branch name for "next release" development: [develop]
Feature branches? [feature/]
Bugfix branches? [bugfix/]
Release branches? [release/]
Hotfix branches? [hotfix/]
Support branches? [support/]
Version tag prefix? []
```

### Release Preparation

- Ensure that all work to be released has been merged into develop.
- Ensure that the `develop` and `master` branches are up to date:

```
$ git pull origin master:master
$ git pull origin develop:develop
```

### Start the Release

```
$ git flow release start [VERSION]
```

Currently, we use the current date for the release version. For example:

```
$ git flow release start 20170815
```

You should now have a release branch called (for this example) `release/20170815`. Push this branch to BitBucket:

```
$ git push origin release/VERSION  # e.g. release/20170815
```

You should now deploy this branch to staging for testing.

## Deploying to Staging

Requirements:

- Your public ssh key on the `vtlaw` user at the host `vtlaw.colab.coop`. If you can `ssh vtlaw@vtlaw.colab.coop` you can deploy to staging.
- You previously ran `./scripts/setup-hooks.sh`. This will create the git alias that you'll use below.
- An open release branch that has been pushed to BitBucket (e.g. `git push origin release/20170815`). See 'Start the release' above.

Once those requirements are met, run one of the following commands:

    $ git deploy-stage  # to deploy the most recently created release/* branch

or:

    $ git deploy-stage release/VERSION  # e.g. release/20170815, to deploy a specific release branch

The staging site is protected with HTTP authentication. See a team member for the credentials.

Test the new release on staging. You can commit small, quick fixes to the release branch as necessary.

## Finishing the Release

Once testing and any fixes have been completed on the release branch, and it's ready for production, finish the release with:

```
$ git flow release finish [VERSION]
```

This will merge the release into master, add an annotated version tag, and merge the master branch back into develop. You'll need to push these merges and the new tag to BitBucket:

```
$ git push origin develop:develop
$ git push --follow-tags origin master:master
```

## Deploying to Production

Requirements:

- An active VPN connection to the VTLAW web hosting environment. (See below for VPN info.)
- Your public ssh key on the `vtlaw` user at the host `11.0.2.90`. If you can `ssh vtlaw@11.0.2.90` you can deploy to production.
- You previously ran `./scripts/setup-hooks.sh`. This will create the git alias that you'll use below.
- A completed release version tag name (e.g. `20170815`). See 'Finish the Release' above.

Production should only be deployed from release tags. To do so, run the deploy script:

```
$ git deploy-prod [VERSION]  # e.g. git deploy-prod 20170815
```

### VPN Server Info

On macos, the VPN type is `Cisco IPSec`.

```
Server Address: 34.224.16.95
Protocol: L2TP/IPsec or PPTP
Username: vtlawadmin
Password: [see Jure]
Shared Secret: [see Jure]
```

[git flow]: http://nvie.com/posts/a-successful-git-branching-model/
[Git flow extensions]: https://github.com/petervanderdoes/gitflow-avh

## Possible deployment failures

Below is a list of historical failures and fixes that have been applied.

- Occasionally output like `The disk hosting
  /opt/www/vtlaw/.config/composer is full, this may be the cause of the
  following exception` may precede a `[RuntimeException]`. In the past
  it has been necessary to remove older backups from
  `/opt/www/vtlaw/backups`, which are created automatically on every
  deployment.
- Sometimes the build will fail because of a neglected prompt on the
  sever during `composer install`. In these cases, it's necessary to SSH
  into the server and review the logged output for additional
  information.
- Always make sure that `$settings['config_readonly'] = FALSE` in
  `settings.php`. This should always be the case, but has come up in the
  past, it will keep config from being deployed and break the build.
- Drush needs to use PHP 7.1 cli in order to avoid failures due to
  missing PHP extensions i.e. `Your requirements could not be resolved to
  an installable set of packages.` etc.

## Rolling Back a Release

**Note:** These steps are untested as of August 2017.

If catastrophe strikes and the site must be rolled back to a previous state, two steps will be required:

- Revert the code to a previous release tag
- Restore a database backup from just before that release

### Reverting Code to a Previous Release

This should be as simple as deploying any release to production.

```
$ git deploy-prod [VERSION]
```

You can use `git fetch --tags && git tag` for a list of release versions. Here's an example output:

```
$ git tag
2017-08-09.1
2017-08-10.1
20170810
20170811.1
20170814
20170814.1
20170815
```

### Restoring a Database Backup

Backups are created automatically before deployment. You can find them on the production server at `/opt/www/vtlaw/backups/`.

If you're connected to the VPN (see above), you can find a list of backups like so:

```
$ ssh vtlaw@11.0.2.90 'ls -la /opt/www/vtlaw/backups'
vtlaw.201708112201.mysql
vtlaw.201708141328.mysql
vtlaw.201708141330.mysql
vtlaw.201708150027.mysql
vtlaw.201708152352.mysql
vtlaw.latest.mysql
```

You can restore one of those backups to the live database using the `mysql` command.

**WARNING:** This is untested! If it works, any new content created after the database backup will be lost.

```
$ ssh vtlaw@11.0.2.90 'mysql --user=vtlawadmin --password=$SQLROOT -h vtlaw-db.ce6vkgevwhup.us-east-1.rds.amazonaws.com vtlaw < /opt/www/vtlaw/backups/vtlaw.[DATESTAMP].mysql'
```

You should match a database backup to the code version to which you plan to revert.

For example, if you are reverting code to tag `20170815`, look for a database backup from that date, such as `vtlaw.201708152352.mysql`.
