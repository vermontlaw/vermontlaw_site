# About this project

Welcome to the developer documentation for the Vermont Law website.

If you have [mkdocs][] installed, run the following from the repository root:

    $ mkdocs serve

You can then visit [http://127.0.0.1:8000/][] in your browser and have a much
better documentation viewing experience.

[mkdocs]: http://www.mkdocs.org/
[http://127.0.0.1:8000/]: http://127.0.0.1:8000/
[composer]: https://getcomposer.org/
