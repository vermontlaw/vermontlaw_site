# Theming

The site uses a custom theme that is a renamed copy of [Emulsify][], a boilerplate theme that integrates [Pattern Lab][] v2 with Drupal 8.

These are early days for the integration of these tools, and as such, this integration isn't optimal. Ideally, the NPM and Composer packages would be at the root level of the project, but this setup works for now.

Currently, compiled assests (including the main CSS file) are _not_ committed to the code repository, and are built on deployment. Thus, most developers will need to run the intial setup below, even when doing non-theming work.

## Intial Setup

Emulsify, and thus this theme, require installation of node.js and PHP dependencies via `npm` and `composer`. The initial setup requires the following commands:

```
$ cd docroot/themes/vtlaw
$ nvm use      # Only if you use nvm for node.js management. Otherwise, use node.js v6.x.
$ npm install  # Also runs composer for the pattern-lab install.
```

## Keeping Dependencies Up-to-date

On occasion, the node and composer dependencies may be updated, though this should happen rarely. If you need to do so, run the above commands again.

## Workflow

This section will be expanded as we work with these tools, but here's the short version:

```
$ cd docroot/themes/vtlaw
$ npm start
```

This will open a local web site for the live-reloading style guide.

Drupal-independent style and component editing will happen mainly via edits to the files in `components/_patterns/`. This setup uses the [Sass][] preprocessor with Sass partials located next to their twig templates. These twig templates are then included in the Drupal-specific templates in `templates/`.

Here's the main thing to remember about this setup. The templates in `components/` should be designed as if this weren't a Drupal project. This means that BEM-style CSS classes, lean markup, and sensible template variable names are encouraged. The Drupal-specific templates and preprocessing functions in the theme will translate Drupalisms into these templates. Of course, this is an aspiration, and reality may dictate that some Drupalisms will leak into the component templates.

## Layout in Drupal

This section documents the Drupal layout technique used on the site. The *presentation* of that layout, implemented via CSS, is not discussed here. For consistency, this layout technique will be called *region view modes*.

In order to avoid complex layout systems like `Panels`, [theme regions][] (part of every theme in Drupal) are used to display variants of the current node.

This technique is used most extensively on *Basic Pages*, but other content types use it as well.

Normally, a node would be displayed in the Content region of the current theme. *Blocks*, which are generally unrelated content, are placed in other regions such as the sidebars, header, and footer.

To place parts of a node in multiple theme regions, we created additional node [view modes][] for the 'Header', 'Content Top', 'Content Bottom', 'Content Aside', and 'Content' theme regions. (View modes are used to display the content from a given entity, like a node, in different ways. Think of them as display variants for a given node.)

We then added blocks that display the current node, using one of the above view modes, in the corresponding theme region. You can see them at `/admin/structure/block`.

In this case, we used the *Entity view (Content)* block type provided by the `ctools` module, but we could also have used a Views block display with a contextual filter set to use the current node id.

With the above settings in place, we can now place different fields from a given node in different theme regions by displaying them in the appropriate view mode.

For example, on a person node, the expertise field is set to display in the `Aside region` view mode (`/admin/structure/types/manage/person/display/aside_region`) and hidden in the `Default` view mode (`/admin/structure/types/manage/person/display`) which is displayed in the main content region.


[Emulsify]: https://github.com/fourkitchens/emulsify
[Pattern Lab]: http://patternlab.io/
[Sass]: http://sass-lang.com/
[theme regions]: https://www.drupal.org/docs/8/theming-drupal-8/adding-regions-to-a-theme
[view modes]: https://www.drupal.org/docs/8/api/entity-api/display-modes-view-modes-and-form-modes
