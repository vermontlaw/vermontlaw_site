# Development

This Drupal 8 site is built with a [composer][]-based workflow, using [drupal-composer][].

# Developer Requirements

- [composer][]
- drush
- A POSIX-compliant development machine (e.g. macOS or Linux). If you use Windows, you'll need either a virtual machine or, possibly, the [Windows Subsystem for Linux][].

You may use a globally installed version of drush or the version included via composer. You can find the composer-installed one in `vendor/bin/drush`. Many of the drush commands in this documententation can replace `drush` with `../vendor/bin/drush`, assuming you are in the `docroot/` directory.

# Initial Developer Setup

1. Clone this repository
2. Open a terminal at the root of the repo
3. Run `./scripts/setup-hooks.sh` to configure deployment commands, git hooks, and other settings.
4. Run `composer install`
5. Copy the file from `docroot/sites/example.settings.local.php` to `docroot/sites/default/settings.local.php` and update the database connection info. Here's an example:

        <?php
        $databases['default']['default'] = array (
          'database' => 'vls',
          'username' => 'root',
          'password' => '',
          'prefix' => '',
          'host' => 'localhost',
          'port' => '3306',
          'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
          'driver' => 'mysql',
        );

        // Disable google analytics.
        $config['google_analytics.settings']['account'] = '';

        // Allow configuration changes.
        $settings['config_readonly'] = FALSE;

Setting up your local web server and database is left as an excercise for the developer. Please note when setting up your web server, though, that this project uses the `docroot` directory as the web root. Standard procedure for using drupal-vm and a base site alias may be provided at a later point.

## Clean Install

During the initial development of the site, as migrated content is prepared and design choices are made, developers be installing the site from scratch.

Install the site with `drush si --config-dir=../config/sync`.

You should now be able to visit `http://YOUR_LOCAL_HOSTNAME/` and see the site with a clean installation using configuration stored in `config/sync`.

To log in as user 1, use `drush uli -l YOUR_LOCAL_HOSTNAME`.

## Syncing Content from the Staging and Live Sites

To pull content from the staging or production servers, you'll need to ensure that your local drush alias is configured correctly. Run `./scripts/setup-hooks.sh` if you haven't recently, and ensure that your *local development hostname* is correct (e.g. `vls.dev`, `vtlaw.dev`, or whatever you use).

The `setup-hooks.sh` script will store some configuration for the `drush @vls.local` alias, allowing you to run the sync commands.

### Sync content from staging

Run the following command:

```
$ drush @vls.local sync-stage
```

### Sync content from production

Ensure that you are connected to the VPN (see [deployment](deployment.md)) and run the following command:

```
$ drush @vls.local sync-prod
```

## Theming

There is dedicated documentation for [Theming][], but since this project does not keep compiled CSS and other assets under version control, most developers will still need to follow the Initial Setup steps from that documentation.

# Ongoing Development

## Feature Branches and Pull Requests

Make code changes using git [feature branches][], based off of the `develop` branch. Name your feature branches after the corresponding ticket name, like VTLAW-40 or VTLAW-32,33,36.

As you work, *make incremental commits*, breaking them up into small chunks of related changes. Use descriptive commit messages.

- Good commit message: `Add and enable the Components module`
- Not so good commit message: `fixes`

When you're ready for code to be reviewed, push your feature branch and [create a pull request][]. Bitbucket will helpfully give you a pull-request link in your terminal after every push.

Name the pull request with the ticket # and name (e.g. VTLAW-000) and provide a helpful description, including any steps needed to test the code in the request or useful explanations for solutions chosen. Be sure to select one or more reviews, who will be notified of the new pull request.

### Keeping Feature Branches Up-to-date with Other Development

If your feature branch is long-lived, odds are good that new features and fixes will be added to the `develop` branch. To include these updates in your feature branch, merge the changes from `develop`. With your feature branch checked out:

```
# Commit or stash any changed files in your working directory.
$ drush cex  # Export any configuration from your work in progress.
$ git fetch origin  # Get the latest commits from the `origin` remote.
$ git merge origin/develop
```

If there are any conflicts in the Drupal configuration settings, git will report a merge conflict, which you'll need to resolve to continue. After the merge is complete, run:

    $ composer install  # Update any packages or modules.
    $ drush cim  # Import any new configuration.

## Reviewing Pull Requests

When possible, reviewers should locally test code changes in pull requests. The easiest way to do so is to check out the feature branch locally, using:

    git checkout -t origin/VTLAW-000   # Be sure to use the correct branch name

Get any updates to modules and other packages using:

    composer install

Then update and add new configuration:

    drush cim

## Mangaging Modules with Composer

See Jeff Geerling's excellent [Tips for Managing Drupal 8 projects with Composer][] article for detailed information.

To add a module, run the following from the repository root:

    composer require drupal/modulename:^1.0

This will download the 8.x-1.x version of the module to `modules/contrib` and add a reference to it in the `composer.json` and `composer.lock` files.

To update modules, please don't update everything at once using `composer update`. Target the modules you wish to update specifically:

    composer update drupal/modulename --with-dependencies

To remove a module, uninstall it, then run:

    composer remove drupal/modulename

### Updating Drupal core

Even though it isn't a module, core is updated similarly:

    composer update drupal/core

### Managing Development dependencies

To be written, once `config_split` or the equivalent is configured.

## Patching Modules with Composer

If you must patch a module for a bug fix or missing functionality, add it to the `patches` section in the `extra` section of the `composer.json` file:

```
"extra": {
    "patches": {
        "drupal/menu_block": {
            "2+ level menu block not limited to active parent": "https://www.drupal.org/files/issues/menu_block-2811337-expand_only_active_tree-29.patch"
        },
        "drupal/shariff": {
            "Allow pseudo field on paragraph entities, too": "https://www.drupal.org/files/issues/paragraphs_support-2892483-2.patch"
        }
    }
}
```

After adding the patch, run `composer update none` to apply the patch to the exact version of the module currently installed.

Only apply patches from the existing drupal.org issues.

## Importing and Exporting Drupal Configuration

This project maintains all Drupal configuration (e.g. content types, Views definitions, module settings, etc.) in code in the `./config/sync` directory. `config_readonly` has been enabled on the production server to prevent configuration changes on the live site.

To modify configuration or evaluate configuration changes by other developers, you'll need to export and import configuration.

### Example: Update the site name

Assuming you had the task of modifying the site name from the current 'Vermont Law School' to, say, 'VLS', you would change the configuration at `/admin/config/system/site-information` on your local development setup (see above) and then export those changes to a feature branch for testing (see 'Feature Branches and Pull Requests' above). Exporting looks like this:

```
$ drush config-export   # or drush cex
```

This will export the changes to the config directory on the file system:

```
$ git diff
diff --git a/config/sync/system.site.yml b/config/sync/system.site.yml
index 28785bb..a329644 100644
--- a/config/sync/system.site.yml
+++ b/config/sync/system.site.yml
@@ -1,5 +1,5 @@
 uuid: 9e00b13e-ea24-48d7-bb7c-ca66818863f8
-name: 'Vermont Law School'
+name: VLS
 mail: info@vermontlaw.edu
 slogan: ''
 page:
```

From there, you would commit the change to git, and either deploy for testing or create a pull request for another developer to review your changes.

Let's assume another developer is reviewing your changes. They would check out your feature branch and then *import* your configuration changes. Importing is also done via drush:

```
$ drush config-import   # or drush cim
```

Any configuration changes found in the `config/sync` directory will be imported into the active configuration for the site.

Incidentally, this configuration import step is part of the deployment scripts for dev, stage, and prod.

[composer]: https://getcomposer.org/
[Theming]: theming.md
[drupal-composer]: https://github.com/drupal-composer/drupal-project
[feature branches]: https://www.atlassian.com/git/tutorials/comparing-workflows#feature-branch-workflow
[create a pull request]: https://www.atlassian.com/git/tutorials/making-a-pull-request
[Tips for Managing Drupal 8 projects with Composer]: https://www.jeffgeerling.com/blog/2017/tips-managing-drupal-8-projects-composer
[Windows Subsystem for Linux]: https://msdn.microsoft.com/commandline/wsl
