# Redirects

The redirect module is installed for Drupal-specific sites, but for redirect patterns, we use web server redirects.

Drupal sites are commonly hosted using Apache, and redirects are added to the `.htaccess` file. This site is hosted using nginx, so redirects must be added to the virtual host configuration.

This is done using a config file included in the repo.

For the development site, that file is `redirects-dev.conf`, and for the production site, it's `redirects-prod.conf`.

These files can contain nginx configuration directives, but are primarily used for redirects. Here's an example for the dev site(s):

```
# redirects-dev.conf

location /directory/ {
    if ($query_string ~ "^name=(\w+),%20(\w+)$") {
        rewrite ^/directory/person /directory/person/%1-%2? redirect;
    }
}
```

Note that after deployment, the nginx server must be [gracefully restarted][] via the `HUP` signal. As of July 2017, this is a manual process.

[gracefully restarted]: http://nginx.org/en/docs/control.html
