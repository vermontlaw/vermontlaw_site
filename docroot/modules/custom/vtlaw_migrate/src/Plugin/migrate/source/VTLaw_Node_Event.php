<?php
/**
 * @file
 * Contains \Drupal\vtlaw_migrate\Plugin\migrate\source\ExampleFile.
 */
namespace Drupal\vtlaw_migrate\Plugin\migrate\source;
use Drupal\migrate\Row;
use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;
use \DateTime;
use \DateTimeZone;

/**
 * Populate additional fields.
 *
 * @MigrateSource(
 *   id = "vtlaw_node_event",
 *   source_provider = "CSV"
 * )
 */

class VTLaw_Node_Event extends CSV {
  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $cal = $dept = $place = array();
    $search = $row->getSourceProperty('VLS _x002d_ Search_x');
    if (isset($search) && $search > '') {
      foreach (explode(';', $search) as $s) {
        if (isset($this->searchToCal[$s])) $cal[] = $this->searchToCal[$s];
        if (isset($this->searchToDept[$s])) $dept[] = $this->searchToDept[$s];
      }
      if (count($cal)) $row->setSourceProperty('calendars', implode(';', $cal));
      else $row->setSourceProperty('calendars', '');
      if (count($dept)) $row->setSourceProperty('departments', implode(';', $dept));
      else $row->setSourceProperty('departments', '');
    }
    else {
      $row->setSourceProperty('calendars', '');
      $row->setSourceProperty('departments', '');
    }
    $location = $row->getSourceProperty('Location');
    if (isset($location) && $location > '') {
      foreach (explode(';', $location) as $l) {
        if (isset($this->locToPlace[$l])) $place[] = $this->locToPlace[$l];
      }
      if (count($place)) $row->setSourceProperty('places', implode(';', $place));
    }

    $desc = $row->getSourceProperty('Description');
    $body = $row->getSourceProperty('Body');
    $full = $row->getSourceProperty('Event Full Description');
    if (!isset($full) || $full == '' || $full == '<p></p>' || $full == '<div></div>') {
        $row->setSourceProperty('Event Full Description', $desc ."\n". $body);
    }

    $s1 = $row->getSourceProperty('EventDate');
    $s2 = $row->getSourceProperty('Date of Event');
    if (!isset($s1) || $s1 == '') $s1 = $s2;
    if ($s1 > '') {
      $date = new DateTime($s1, new DateTimeZone('UTC'));
      $date->setTimezone(new DateTimeZone('America/New_York'));
      $row->setSourceProperty('EventDate', $date->format('Y-m-d\TH:i:s'));
    }

    $s1 = $row->getSourceProperty('EndDate');
    $s2 = $row->getSourceProperty('Event End Date');
    if (!isset($s1) || $s1 == '') $s1 = $s2;
    if ($s1 > '') {
      $date = new DateTime($s1, new DateTimeZone('UTC'));
      $date->setTimezone(new DateTimeZone('America/New_York'));
      $row->setSourceProperty('EndDate', $date->format('Y-m-d\TH:i:s'));
    }

    parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    $fields['calendars'] = $this->t('calendars gleaned from search terms');
    $fields['departments'] = $this->t('calendars gleaned from search terms');
    $fields['places'] = $this->t('places gleaned from location');
    return $fields;
  }

  // Map search IDs to calendar IDs
  private $searchToCal = array(
    'e57d1de1-4cc3-4930-ad82-dc0bd4ef5f38' => 'd45c2498-60eb-11e7-907b-a6006ad3dba0', //2015 Fall DL
    'd83dc1ff-aa52-4e73-a59e-9944f15d24a2' => 'd45c2498-60eb-11e7-907b-a6006ad3dba0', //2016 Fall DL
    '65f49c60-c593-4ec3-a2af-2dabdee12bac' => '65f49c60-c593-4ec3-a2af-2dabdee12bac', //2013 Fall
    'd0354919-1de0-447e-a7c6-8728370027ed' => '65f49c60-c593-4ec3-a2af-2dabdee12bac', //2014 Fall
    '90d8cc79-a487-4872-9aa5-ba8afb5bbce6' => '65f49c60-c593-4ec3-a2af-2dabdee12bac', //2015 Fall
    '196d84bb-579f-4165-9556-d6ef5ccc0cf0' => '65f49c60-c593-4ec3-a2af-2dabdee12bac', //2016 Fall
    '65f49c60-c593-4ec3-a2af-2dabdee12bac' => '65f49c60-c593-4ec3-a2af-2dabdee12bac', //Academic Calendar Residential - Fall Semester 2013
    'd0354919-1de0-447e-a7c6-8728370027ed' => '65f49c60-c593-4ec3-a2af-2dabdee12bac', //Academic Calendar Residential - Fall Semester 2014
    '90d8cc79-a487-4872-9aa5-ba8afb5bbce6' => '65f49c60-c593-4ec3-a2af-2dabdee12bac', //Academic Calendar Residential - Fall Semester 2015
    '196d84bb-579f-4165-9556-d6ef5ccc0cf0' => '65f49c60-c593-4ec3-a2af-2dabdee12bac', //Academic Calendar Residential - Fall Semester 2016
    'f71a8bac-aed5-439c-b60f-b0e2d6d0b810' => 'd45c2880-60eb-11e7-907b-a6006ad3dba0', //2016 Spring DL
    '91926c7f-c23e-4305-857a-02fe4311b59a' => 'd45c2880-60eb-11e7-907b-a6006ad3dba0', //2017 Spring DL
    '85a83646-27bf-4280-8d03-51a266b0d50d' => '85a83646-27bf-4280-8d03-51a266b0d50d', //2014 Spring
    'bceb067e-d538-4bfb-9f8a-bc99b6bd74ce' => '85a83646-27bf-4280-8d03-51a266b0d50d', //2015 Spring
    'e9be3fe2-8caa-48ac-8948-f10509cf3425' => '85a83646-27bf-4280-8d03-51a266b0d50d', //2016 Spring
    '5fa193df-02fa-4164-82b3-005b237506c8' => '85a83646-27bf-4280-8d03-51a266b0d50d', //2017 Spring
    '85a83646-27bf-4280-8d03-51a266b0d50d' => '85a83646-27bf-4280-8d03-51a266b0d50d', //Academic Calendar Residential - Spring Semester 2014
    'bceb067e-d538-4bfb-9f8a-bc99b6bd74ce' => '85a83646-27bf-4280-8d03-51a266b0d50d', //Academic Calendar Residential - Spring Semester 2015
    'e9be3fe2-8caa-48ac-8948-f10509cf3425' => '85a83646-27bf-4280-8d03-51a266b0d50d', //Academic Calendar Residential - Spring Semester 2016
    '5fa193df-02fa-4164-82b3-005b237506c8' => '85a83646-27bf-4280-8d03-51a266b0d50d', //Academic Calendar Residential - Spring Semester 2017
    '911891c2-b14d-4f81-a33d-c99831132c8b' => '911891c2-b14d-4f81-a33d-c99831132c8b', //2015 Summer DL
    '3fb98100-0d06-4a8e-b20a-1773cdcc610a' => '911891c2-b14d-4f81-a33d-c99831132c8b', //2016 Summer DL
    'c73f2276-a5da-4156-9d72-42e3a9db9976' => '911891c2-b14d-4f81-a33d-c99831132c8b', //2017 Summer DL
    '911891c2-b14d-4f81-a33d-c99831132c8b' => '911891c2-b14d-4f81-a33d-c99831132c8b', //Academic Calendar Online - Summer Semester 2015
    '3fb98100-0d06-4a8e-b20a-1773cdcc610a' => '911891c2-b14d-4f81-a33d-c99831132c8b', //Academic Calendar Online - Summer Semester 2016
    'c73f2276-a5da-4156-9d72-42e3a9db9976' => '911891c2-b14d-4f81-a33d-c99831132c8b', //Academic Calendar Online - Summer Semester 2017
    'e522613f-2b28-45e4-a2e1-c420bc75d85f' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //2013 Summer
    'e4650506-accf-452d-81bf-4d12df78dc22' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //2014 Summer
    '47056a73-1f1e-4049-8fa2-2116aa5ed982' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //2015 Summer
    'bc7db2aa-e4fa-449e-9b5b-195dc2880892' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //2016 Summer
    '6ff18933-1950-4ce7-b272-9cc76d52746d' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //2017 Summer
    'e522613f-2b28-45e4-a2e1-c420bc75d85f' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //Academic Calendar Residential - Summer Session 2013
    'e4650506-accf-452d-81bf-4d12df78dc22' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //Academic Calendar Residential - Summer Session 2014
    '47056a73-1f1e-4049-8fa2-2116aa5ed982' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //Academic Calendar Residential - Summer Session 2015
    'bc7db2aa-e4fa-449e-9b5b-195dc2880892' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //Academic Calendar Residential - Summer Session 2016
    '6ff18933-1950-4ce7-b272-9cc76d52746d' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //Academic Calendar Residential - Summer Session 2017
    '21e08004-29d6-4904-9582-341e55af3bf8' => 'e522613f-2b28-45e4-a2e1-c420bc75d85f', //Summer Session
    'efacf2b9-8e73-4fb2-b628-e6b3e5ab3ab8' => 'efacf2b9-8e73-4fb2-b628-e6b3e5ab3ab8', //Admitted Students Calendar DL
    '71cb1150-4a1f-448b-8245-f1549ee5c81b' => '71cb1150-4a1f-448b-8245-f1549ee5c81b', //Admitted Students Calendar Residential
  );

  // Map search IDs to department IDs
  private $searchToDept = array(
    '01175520-fb53-4252-946c-277cf7425a1f' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '0c583e18-7fa3-41eb-afab-58c29367c74f' => 'b171a34a-b57d-4359-9db8-cd6ae607b368',
    '0dea2ad4-3f9c-4330-a84c-e7db9d1db155' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    '0f83f7f5-88c3-4b8e-a619-b3783ebf2b6b' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    '0ffa86b1-c1d3-4336-9ecf-a4d6cb05b060' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '101f3f02-2f64-438a-9196-de1513f07205' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '1020eacb-53dd-429f-a57c-42c981605625' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
    '192e3d16-3f75-4c7f-9ce6-65c45d70bf2d' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    '2159c86f-f52d-4dc7-9661-6ee7efff361e' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    '21c82a38-de92-4ccc-a250-5528a503bb2b' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    '236af3a5-1dfc-49ed-b102-d0db00182da7' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '2786cefd-40a6-4017-8ace-72febae77d7d' => 'eda3642c-a12f-420d-811a-110c11f08578',
    '2a1eaea6-5081-4566-9cb5-e4a78459fcdf' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    '2cec0a66-db6c-4e73-8eb9-7ba71b22589f' => 'd37b2429-16d9-4fc4-b72e-eed5f15f816a',
    '2f515e0e-68fb-44da-9fbe-2dcc7ad2c04f' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    '31466efb-a771-4e5c-8636-a7c13b502f31' => 'b543c6f0-3fd1-11e7-a919-92ebcb67fe33',
    '439e12c4-6ece-45fc-be51-17747928babc' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '4d1782f1-3266-4aa9-a262-df1a177e3d94' => '7926a6a0-fe8e-453c-8837-075233f219de',
    '4d30f0cb-0050-491a-82d8-1f0c9a68bd5c' => '7926a6a0-fe8e-453c-8837-075233f219de',
    '4f079b0c-e429-497b-a839-b9abf13c1504' => 'eda3642c-a12f-420d-811a-110c11f08578',
    '50b89661-5e72-4785-8585-303dd02ba71e' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    '51829b10-e7c4-4088-982b-c846c247893e' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    '556cbe3c-6fb9-452c-9add-a4cc60b4ea62' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '5e3f03f5-557c-4ff8-a6c2-ab55e7d8153a' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '66fcc307-1b0a-49e3-9484-bcf8714e769f' => '801b292d-d400-4342-ab64-1a75ee2cf45f',
    '6799001f-d612-4f26-912f-ede2aa18da22' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    '692eab03-0bef-49e3-863d-9f5688b3007e' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    '7216946f-aa30-4aa1-836c-7b21581640a1' => 'f50c1620-3a68-11e7-a919-92ebcb67fe33',
    '72a413ec-379d-45e7-a9c8-9fc109284260' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    '754bfffe-5756-452a-a7fa-3cebcdff5205' => 'b543c43e-3fd1-11e7-a919-92ebcb67fe33',
    '76949622-7bb7-43c8-aabe-a83a542c18af' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '7d412be9-b2f0-4e81-b60d-0e835a678e80' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    '8057b59b-47e5-4978-93d4-7ab5cfb4d036' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '84d61db7-190f-4db1-b41d-c669c7a9035d' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    '85207d3c-5ed6-4708-810a-1bcd469df3c4' => '039026c2-3a6a-11e7-a919-92ebcb67fe33',
    '85f3fc2b-abfe-48c6-b86e-f027e592cd31' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    '87208647-62ec-45e8-afdd-3e2fa18a450d' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '8d9efe92-f29d-482e-b4ad-cc18517c2630' => 'e4423ac2-3a68-11e7-a919-92ebcb67fe33',
    '9255b5d7-1e10-4b21-aea0-f80b76af2b8e' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
    '92aaf9e1-e11b-43cc-bcaf-8264624536f5' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '96c4ba22-c60f-4a31-aef7-ad0b0ab2ba3f' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    '9985ac59-094e-4d32-8537-77b73759314c' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    'a42bd434-d000-4e5f-8fdf-bde0eda86ae9' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    'a5231044-5a65-4c23-a26d-146880d0d034' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'a978d47d-8d00-415a-842e-f1bbafc2d424' => '7926a6a0-fe8e-453c-8837-075233f219de',
    'a98316d4-0e6c-428a-8767-ca40bcf9e782' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    'b5df066c-fcd3-4a7c-9302-ba97d0a59fac' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    'b6b4773c-addd-45db-897f-92074c2b04cd' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    'bca93f57-a24a-48b6-b75f-6d11ce777b2e' => 'f2283258-85f1-415b-8fde-d59f56ab4f8a',
    'bd7ab307-b0ff-4e4a-8b5b-f4a704994f61' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    'c083aff9-3106-4beb-9225-a33d78158256' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    'c09bfe31-e99f-433f-ae89-804150ff3cd4' => 'eda3642c-a12f-420d-811a-110c11f08578',
    'c94a517a-16e2-49d4-84da-f83b4ce01d17' => 'b543c6f0-3fd1-11e7-a919-92ebcb67fe33',
    'c9d813ff-c5e2-484b-8156-5fdf58ac721d' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'ce8891e7-cf2d-4adf-937a-bb7498696eba' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    'ced25981-5b16-4e57-a1bb-edbed34dd493' => 'df3aa630-3a69-11e7-a919-92ebcb67fe33',
    'cfda6aae-a82a-4c7c-bfa5-4c8c18547c54' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    'd0e831ff-0391-4dfc-9289-8840b3d6836d' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'd1cab906-a453-485d-8a98-4714cc524209' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    'd1f5a2eb-ceea-4e11-93fe-d00e58584161' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    'd4cbadaa-b1d7-4c0f-b0f7-bdf6c7cc5aeb' => 'eda3642c-a12f-420d-811a-110c11f08578',
    'd5b0eb83-0245-4df4-af76-83fcc85d207d' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
    'd6983a7c-53c9-4d5b-8248-dd6f6e6b8a90' => 'ef44685e-3a69-11e7-a919-92ebcb67fe33',
    'de3121e7-e665-4e6f-8b1d-0a57c118b2af' => '817099bd-c4bb-415b-b669-45a8866792bd',
    'de89c763-818f-42c1-abef-eb4398bb78ab' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    'e34bacfb-02c2-4c64-bf48-2b61cd6526c3' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
    'e4b39928-be22-4765-adcf-589b7f7548ad' => 'e4423ac2-3a68-11e7-a919-92ebcb67fe33',
    'e9785d3c-1ec5-4326-bc43-2d53a7f62e62' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    'ec9aca40-2883-4d48-b5ca-0bee06c564cc' => '6cc79cd9-6d3a-4a61-9e8b-5085c3f94aaf',
    'ed5def61-341e-4dab-bb74-3c657a039db7' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'ee74117d-353d-4121-abb3-20618b608e33' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'ee7e0fe4-8d86-4ff1-8a64-4e37d2780c56' => '613f8094-259a-4315-8a33-f87ae51597df',
    'efbfd006-08c4-442f-9f29-c5514ef7a7bf' => 'b543cd8a-3fd1-11e7-a919-92ebcb67fe33',
    'f19eb482-50ce-4401-b493-5e229d5a4b2d' => '71f74fea-d6db-4887-97a9-1688441a743c',
    'f1a46b0a-9a19-4326-976f-4936c6f00347' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    'f6214f9e-7420-4145-8bad-13780f7f6305' => 'f50c1620-3a68-11e7-a919-92ebcb67fe33',
    'f64b3652-f24f-4f6e-befe-54c4b7312e5d' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    'febc04d8-990f-44d9-adb7-5a541977ace3' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
  );

  // Map location names to place IDs
  private $locToPlace = array(
    '107 Oakes Hall' => '0135e0c0-3a78-11e7-a919-92ebcb67fe33',
    '110 Oakes Hall' => '0135e43a-3a78-11e7-a919-92ebcb67fe33',
    '190 Chelsea Street' => '0c8024a1-51fd-4db4-b31e-a3ec7cd72882',
    '208 Oakes Hall' => '0135ed72-3a78-11e7-a919-92ebcb67fe33',
    'At the Pitch, Rte 110, Tunbridge' => '01361522-3a78-11e7-a919-92ebcb67fe33',
    "Barrister's Book Shop" => "01357ebe-3a78-11e7-a919-92ebcb67fe33",
    "Barrister's Bookstore" => "01357ebe-3a78-11e7-a919-92ebcb67fe33",
    'Chase' => '4a176a5b-6c16-4d49-8fd4-901ead1e1af3',
    'Chase Auditorium' => '4a176a5b-6c16-4d49-8fd4-901ead1e1af3',
    'Chase Breezeway' => '01358422-3a78-11e7-a919-92ebcb67fe33',
    'Chase Center' => '4a176a5b-6c16-4d49-8fd4-901ead1e1af3',
    'Chase Center, Vermont Law School' => '4a176a5b-6c16-4d49-8fd4-901ead1e1af3',
    'Chase Center, Vermont Law School and Capitol Plaza Hotel, Montpelier, Vermont' => '4a176a5b-6c16-4d49-8fd4-901ead1e1af3',
    'Chase Community Center' => '4a176a5b-6c16-4d49-8fd4-901ead1e1af3',
    'Chase Loft' => '01358f3a-3a78-11e7-a919-92ebcb67fe33',
    'Cornell Library' => '724d58c6-880e-40c3-bd79-091fa8de4699',
    'Cornell Library Seminar Room' => '0135a0f6-3a78-11e7-a919-92ebcb67fe33',
    'Cornell Library Steps' => '0135980e-3a78-11e7-a919-92ebcb67fe33',
    'Cornell Seminar' => '0135a0f6-3a78-11e7-a919-92ebcb67fe33',
    'Cornell Seminar Room' => '0135a0f6-3a78-11e7-a919-92ebcb67fe33',
    'Cornell Seminar Room (Library)' => '0135a0f6-3a78-11e7-a919-92ebcb67fe33',
    'Cornell Seminar Room, Cornell Library' => '0135a0f6-3a78-11e7-a919-92ebcb67fe33',
    'Cornell Seminar Room, Vermont Law School' => '0135a0f6-3a78-11e7-a919-92ebcb67fe33',
    'Crossroads' => '0135a2a4-3a78-11e7-a919-92ebcb67fe33',
    'Crossroads Bar & Grille' => '0135a2a4-3a78-11e7-a919-92ebcb67fe33',
    'Crossroads Bar and Grill' => '0135a2a4-3a78-11e7-a919-92ebcb67fe33',
    'Debevoise 101' => '0135aaf6-3a78-11e7-a919-92ebcb67fe33',
    'Debevoise Back Lawn' => '0135acb8-3a78-11e7-a919-92ebcb67fe33',
    'Debevoise Front Lawn' => '0135b65e-3a78-11e7-a919-92ebcb67fe33',
    'Debevoise Lawn' => '0135b65e-3a78-11e7-a919-92ebcb67fe33',
    'ELC Conf Rm 216' => '0135bbae-3a78-11e7-a919-92ebcb67fe33',
    'Elmira, New York' => '',
    'EPRI, 1325 G Street NW, Washington DC 20005' => '013616b2-3a78-11e7-a919-92ebcb67fe33',
    'Fitness Center - Fitness 105' => '0135bf3c-3a78-11e7-a919-92ebcb67fe33',
    'Front Steps of Debevoise' => '0135b80c-3a78-11e7-a919-92ebcb67fe33',
    'Hoff Lounge' => '0135c108-3a78-11e7-a919-92ebcb67fe33',
    'Hoff Lounge, Oakes Hall' => '0135c108-3a78-11e7-a919-92ebcb67fe33',
    'Jonathon B. Chase Community Center' => '4a176a5b-6c16-4d49-8fd4-901ead1e1af3',
    'Kismet in Montpelier' => '0135d134-3a78-11e7-a919-92ebcb67fe33',
    'Library Quad' => '01359642-3a78-11e7-a919-92ebcb67fe33',
    "Lion's Beach (Paine's Beach)" => '0135fe52-3a78-11e7-a919-92ebcb67fe33',
    'Map Room' => '0135cf4a-3a78-11e7-a919-92ebcb67fe33',
    'Map Room (Debevoise)' => '0135cf4a-3a78-11e7-a919-92ebcb67fe33',
    'Map Room 103' => '0135cf4a-3a78-11e7-a919-92ebcb67fe33',
    'Map Room on the 1st floor of Debevoise Hall' => '0135cf4a-3a78-11e7-a919-92ebcb67fe33',
    'Map Room, Debevoise Hall' => '0135cf4a-3a78-11e7-a919-92ebcb67fe33',
    'Map Room, Vermont Law School' => '0135cf4a-3a78-11e7-a919-92ebcb67fe33',
    'New York, New York' => '0135d846-3a78-11e7-a919-92ebcb67fe33',
    'Nina Thomas Classroom' => '0135db34-3a78-11e7-a919-92ebcb67fe33',
    'Nina Thomas Classroom 102' => '0135db34-3a78-11e7-a919-92ebcb67fe33',
    'Nina Thomas Room' => '0135db34-3a78-11e7-a919-92ebcb67fe33',
    'Oakes 007' => '0135dd5a-3a78-11e7-a919-92ebcb67fe33',
    'Oakes 012' => '0135df26-3a78-11e7-a919-92ebcb67fe33',
    'Oakes 107' => '0135e0c0-3a78-11e7-a919-92ebcb67fe33',
    'Oakes 109' => '0135e28c-3a78-11e7-a919-92ebcb67fe33',
    'Oakes 110' => '0135e43a-3a78-11e7-a919-92ebcb67fe33',
    'Oakes 207' => '0135eb10-3a78-11e7-a919-92ebcb67fe33',
    'Oakes 208' => '0135ed72-3a78-11e7-a919-92ebcb67fe33',
    'Oakes 210' => '0135ef34-3a78-11e7-a919-92ebcb67fe33',
    'Oakes 211' => '0135f0e2-3a78-11e7-a919-92ebcb67fe33',
    'Oakes Hall' => '6f4e958c-558d-4371-a510-88a69ac7af44',
    'Oakes Hall 107' => '0135e0c0-3a78-11e7-a919-92ebcb67fe33',
    'Oakes Hall Room 107, Vermont Law School' => '0135e0c0-3a78-11e7-a919-92ebcb67fe33',
    'Oakes Hall Room 211' => '0135f0e2-3a78-11e7-a919-92ebcb67fe33',
    'Off Campus' => '0135f5ce-3a78-11e7-a919-92ebcb67fe33',
    'Off Site' => '0135f5ce-3a78-11e7-a919-92ebcb67fe33',
    'Off-Campus' => '0135f5ce-3a78-11e7-a919-92ebcb67fe33',
    'Outdoor Classroom' => '0135fc40-3a78-11e7-a919-92ebcb67fe33',
    "Paine's Beach" => '0135fe52-3a78-11e7-a919-92ebcb67fe33',
    'South Royalton Green' => '01360fd2-3a78-11e7-a919-92ebcb67fe33',
    'South Royalton Town Green' => '01360fd2-3a78-11e7-a919-92ebcb67fe33',
    'South Royalton Village Green' => '01360fd2-3a78-11e7-a919-92ebcb67fe33',
    'The Green, South Royalton, VT' => '01360fd2-3a78-11e7-a919-92ebcb67fe33',
    'The Map Room' => '0135cf4a-3a78-11e7-a919-92ebcb67fe33',
    'Washington D.C.' => '013616b2-3a78-11e7-a919-92ebcb67fe33',
    'Washington, D.C.' => '013616b2-3a78-11e7-a919-92ebcb67fe33',
    'Washington, DC' => '013616b2-3a78-11e7-a919-92ebcb67fe33',
    'Waterman' => '19d712b4-6017-4cb7-8515-50867edb865c',
    'Waterman Committee' => '01361cde-3a78-11e7-a919-92ebcb67fe33',
    'Yates Common 104' => '013620bc-3a78-11e7-a919-92ebcb67fe33',
    'Yates Common Room' => '013620bc-3a78-11e7-a919-92ebcb67fe33',
  );
}
