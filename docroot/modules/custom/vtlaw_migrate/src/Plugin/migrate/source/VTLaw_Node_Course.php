<?php
/**
 * @file
 * Contains \Drupal\vtlaw_migrate\Plugin\migrate\source\ExampleFile.
 */
namespace Drupal\vtlaw_migrate\Plugin\migrate\source;
use Drupal\migrate\Row;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;
use Drupal\redirect\Entity\Redirect;

/**
 * @MigrateSource(
 *   id = "vtlaw_node_course",
 *   source_provider = "CSV"
 * )
 */

class VTLaw_Node_Course extends CSV {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    // Create a redirect from the legacy URL, if we have the necessary info.
    $legacy_id = $row->getSourceProperty('ID');
    if ($legacy_id > 0) {
      $subject = str_replace(array(' ', ','), array('-', ''), strtolower($row->getSourceProperty('Subject')));
      $code = strtolower($row->getSourceProperty('Course Code'));
      Redirect::create([
        'redirect_source' => "academics/courses/course/$legacy_id",
        'redirect_redirect' => "internal:/academics/courses/$subject/$code",
        'language' => 'und',
        'status_code' => '301',
      ])->save();
    }

    parent::prepareRow($row);
  }
}
