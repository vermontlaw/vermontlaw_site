<?php
/**
 * @file
 * Contains \Drupal\vtlaw_migrate\Plugin\migrate\source\ExampleFile.
 */
namespace Drupal\vtlaw_migrate\Plugin\migrate\source;
use Drupal\migrate\Row;
use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;

/**
 * Populate additional fields.
 *
 * @MigrateSource(
 *   id = "vtlaw_node_person",
 *   source_provider = "CSV"
 * )
 */

class VTLaw_Node_Person extends CSV {
  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $depts = $row->getSourceProperty('VLS Suborganizations');
    $search = (string) $row->getSourceProperty('VLS _x002d_ Search_x');
    if ($search > '') {
      foreach (explode(';', $search) as $s) {
        if (isset($this->searchToDept[$s]) && strpos($depts, $this->searchToDept[$s]) === FALSE) {
          $depts .= ';'. $this->searchToDept[$s];
        }
      }
    }
    $row->setSourceProperty('departments', trim($depts, ';'));
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    $fields['departments'] = $this->t('combined suborganizations and search terms');
    return $fields;
  }

  // Map search IDs to department IDs
  private $searchToDept = array(
    '01175520-fb53-4252-946c-277cf7425a1f' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '0c583e18-7fa3-41eb-afab-58c29367c74f' => 'b171a34a-b57d-4359-9db8-cd6ae607b368',
    '0dea2ad4-3f9c-4330-a84c-e7db9d1db155' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    '0f83f7f5-88c3-4b8e-a619-b3783ebf2b6b' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    '0ffa86b1-c1d3-4336-9ecf-a4d6cb05b060' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '101f3f02-2f64-438a-9196-de1513f07205' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '1020eacb-53dd-429f-a57c-42c981605625' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
    '192e3d16-3f75-4c7f-9ce6-65c45d70bf2d' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    '2159c86f-f52d-4dc7-9661-6ee7efff361e' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    '21c82a38-de92-4ccc-a250-5528a503bb2b' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    '236af3a5-1dfc-49ed-b102-d0db00182da7' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '2786cefd-40a6-4017-8ace-72febae77d7d' => 'eda3642c-a12f-420d-811a-110c11f08578',
    '2a1eaea6-5081-4566-9cb5-e4a78459fcdf' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    '2cec0a66-db6c-4e73-8eb9-7ba71b22589f' => 'd37b2429-16d9-4fc4-b72e-eed5f15f816a',
    '2f515e0e-68fb-44da-9fbe-2dcc7ad2c04f' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    '31466efb-a771-4e5c-8636-a7c13b502f31' => 'b543c6f0-3fd1-11e7-a919-92ebcb67fe33',
    '439e12c4-6ece-45fc-be51-17747928babc' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '4d1782f1-3266-4aa9-a262-df1a177e3d94' => '7926a6a0-fe8e-453c-8837-075233f219de',
    '4d30f0cb-0050-491a-82d8-1f0c9a68bd5c' => '7926a6a0-fe8e-453c-8837-075233f219de',
    '4f079b0c-e429-497b-a839-b9abf13c1504' => 'eda3642c-a12f-420d-811a-110c11f08578',
    '50b89661-5e72-4785-8585-303dd02ba71e' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    '51829b10-e7c4-4088-982b-c846c247893e' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    '556cbe3c-6fb9-452c-9add-a4cc60b4ea62' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '5e3f03f5-557c-4ff8-a6c2-ab55e7d8153a' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '66fcc307-1b0a-49e3-9484-bcf8714e769f' => '801b292d-d400-4342-ab64-1a75ee2cf45f',
    '6799001f-d612-4f26-912f-ede2aa18da22' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    '692eab03-0bef-49e3-863d-9f5688b3007e' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    '7216946f-aa30-4aa1-836c-7b21581640a1' => 'f50c1620-3a68-11e7-a919-92ebcb67fe33',
    '72a413ec-379d-45e7-a9c8-9fc109284260' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    '754bfffe-5756-452a-a7fa-3cebcdff5205' => 'b543c43e-3fd1-11e7-a919-92ebcb67fe33',
    '76949622-7bb7-43c8-aabe-a83a542c18af' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '7d412be9-b2f0-4e81-b60d-0e835a678e80' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    '8057b59b-47e5-4978-93d4-7ab5cfb4d036' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    '84d61db7-190f-4db1-b41d-c669c7a9035d' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    '85207d3c-5ed6-4708-810a-1bcd469df3c4' => '039026c2-3a6a-11e7-a919-92ebcb67fe33',
    '85f3fc2b-abfe-48c6-b86e-f027e592cd31' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    '87208647-62ec-45e8-afdd-3e2fa18a450d' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '8d9efe92-f29d-482e-b4ad-cc18517c2630' => 'e4423ac2-3a68-11e7-a919-92ebcb67fe33',
    '9255b5d7-1e10-4b21-aea0-f80b76af2b8e' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
    '92aaf9e1-e11b-43cc-bcaf-8264624536f5' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    '96c4ba22-c60f-4a31-aef7-ad0b0ab2ba3f' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    '9985ac59-094e-4d32-8537-77b73759314c' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    'a42bd434-d000-4e5f-8fdf-bde0eda86ae9' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    'a5231044-5a65-4c23-a26d-146880d0d034' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'a978d47d-8d00-415a-842e-f1bbafc2d424' => '7926a6a0-fe8e-453c-8837-075233f219de',
    'a98316d4-0e6c-428a-8767-ca40bcf9e782' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    'b5df066c-fcd3-4a7c-9302-ba97d0a59fac' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    'b6b4773c-addd-45db-897f-92074c2b04cd' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    'bca93f57-a24a-48b6-b75f-6d11ce777b2e' => 'f2283258-85f1-415b-8fde-d59f56ab4f8a',
    'bd7ab307-b0ff-4e4a-8b5b-f4a704994f61' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    'c083aff9-3106-4beb-9225-a33d78158256' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    'c09bfe31-e99f-433f-ae89-804150ff3cd4' => 'eda3642c-a12f-420d-811a-110c11f08578',
    'c94a517a-16e2-49d4-84da-f83b4ce01d17' => 'b543c6f0-3fd1-11e7-a919-92ebcb67fe33',
    'c9d813ff-c5e2-484b-8156-5fdf58ac721d' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'ce8891e7-cf2d-4adf-937a-bb7498696eba' => '82b45adc-2598-4681-a1b3-723e7e8a17f5',
    'ced25981-5b16-4e57-a1bb-edbed34dd493' => 'df3aa630-3a69-11e7-a919-92ebcb67fe33',
    'cfda6aae-a82a-4c7c-bfa5-4c8c18547c54' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    'd0e831ff-0391-4dfc-9289-8840b3d6836d' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'd1cab906-a453-485d-8a98-4714cc524209' => 'f1c453e9-7b6c-4c40-853f-856cfb215827',
    'd1f5a2eb-ceea-4e11-93fe-d00e58584161' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    'd4cbadaa-b1d7-4c0f-b0f7-bdf6c7cc5aeb' => 'eda3642c-a12f-420d-811a-110c11f08578',
    'd5b0eb83-0245-4df4-af76-83fcc85d207d' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
    'd6983a7c-53c9-4d5b-8248-dd6f6e6b8a90' => 'ef44685e-3a69-11e7-a919-92ebcb67fe33',
    'de3121e7-e665-4e6f-8b1d-0a57c118b2af' => '817099bd-c4bb-415b-b669-45a8866792bd',
    'de89c763-818f-42c1-abef-eb4398bb78ab' => 'aa0e61c8-3a68-11e7-a919-92ebcb67fe33',
    'e34bacfb-02c2-4c64-bf48-2b61cd6526c3' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
    'e4b39928-be22-4765-adcf-589b7f7548ad' => 'e4423ac2-3a68-11e7-a919-92ebcb67fe33',
    'e9785d3c-1ec5-4326-bc43-2d53a7f62e62' => '72a413ec-379d-45e7-a9c8-9fc109284260',
    'ec9aca40-2883-4d48-b5ca-0bee06c564cc' => '6cc79cd9-6d3a-4a61-9e8b-5085c3f94aaf',
    'ed5def61-341e-4dab-bb74-3c657a039db7' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'ee74117d-353d-4121-abb3-20618b608e33' => '4f025c21-c10a-4df2-bce3-b1b1c768960c',
    'ee7e0fe4-8d86-4ff1-8a64-4e37d2780c56' => '613f8094-259a-4315-8a33-f87ae51597df',
    'efbfd006-08c4-442f-9f29-c5514ef7a7bf' => 'b543cd8a-3fd1-11e7-a919-92ebcb67fe33',
    'f19eb482-50ce-4401-b493-5e229d5a4b2d' => '71f74fea-d6db-4887-97a9-1688441a743c',
    'f1a46b0a-9a19-4326-976f-4936c6f00347' => '95d4928f-0d84-44e9-a0eb-f95f0e2d28af',
    'f6214f9e-7420-4145-8bad-13780f7f6305' => 'f50c1620-3a68-11e7-a919-92ebcb67fe33',
    'f64b3652-f24f-4f6e-befe-54c4b7312e5d' => 'b543c812-3fd1-11e7-a919-92ebcb67fe33',
    'febc04d8-990f-44d9-adb7-5a541977ace3' => 'b543ca06-3fd1-11e7-a919-92ebcb67fe33',
  );
}
