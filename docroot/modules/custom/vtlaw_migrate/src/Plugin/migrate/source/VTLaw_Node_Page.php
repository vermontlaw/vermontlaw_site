<?php
/**
 * @file
 * Contains \Drupal\vtlaw_migrate\Plugin\migrate\source\ExampleFile.
 */
namespace Drupal\vtlaw_migrate\Plugin\migrate\source;
use Drupal\migrate\Row;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;
use Drupal\redirect\Entity\Redirect;

/**
 * Scrape additional data from pages.
 *
 * @MigrateSource(
 *   id = "vtlaw_node_page",
 *   source_provider = "CSV"
 * )
 */

class VTLaw_Node_Page extends CSV {
  private $inline_images = array();

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $result = db_query("SHOW TABLES LIKE 'migrate_map_page_inline_images'");
    foreach ($result as $r) $table_exists = TRUE;
    if ($table_exists) {
      // Look up the relative paths of all the previously imported inline images.
      $result = db_query("SELECT sourceid1,uuid,uri FROM migrate_map_page_inline_images i JOIN file_managed f ON fid = destid1");
      foreach ($result as $r) $this->inline_images[$r->sourceid1] = array(
        'uuid' => $r->uuid,
        'url' => file_url_transform_relative(file_create_url($r->uri)),
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Scrape additional data from pages.
    $url = $row->getSourceProperty('url');
    /*$file = file_get_contents($url);
    $dom = new \DOMDocument();
    $dom->loadHTML($file);
    // Process the page title.
    $temp_title = $row->getSourceProperty('temp_title');
    foreach ($dom->getElementsByTagName('title') as $title) {
      $t = trim(str_replace(array("\r", "\n"), '', $title->textContent));
      if ($t == 'Error' || $t == '') {
        break;
      } elseif (substr($t, -21) == ' - Vermont Law School') {
        $t = substr($t, 0, -21);
      }
      $temp_title = $t;
      break;
    }
    $row->setSourceProperty('title', $temp_title);
    // Process the title image, if any.
    if (preg_match('/<div class="bannerImg" style="background: url\(([^\)]*)/',$file,$matches)) {
      $row->setSourceProperty('title_image', trim($matches[1], "'"));
    }
    // Process the sidebar links, if any.
    if (preg_match('/<div class="groupheader item medium">.*<\/ul>/', $file, $matches)) {
      $row->setSourceProperty('sidebar_links', $matches[0]);
    }
    // Process the section name.
    if (preg_match('/<div id="desktop">\s*(.*)\s*<\/div>/', $file, $matches)) {
      $row->setSourceProperty('section', trim($matches[1]));
    }*/

    // Preprocess the HTML of the body text.
    $body = $row->getSourceProperty('content');
    if (!isset($body) || $body == '') $body = "This page intentionally left blank.";
    $pattern = array('/<(\/?)b>/', '/<\/?div[^>]*>/', '/<\/?font[^>]*>/', '/<(\/?)h1>/', '/<\/?span[^>]*>/', '/<style>\s*.*\s*.*\s*<\/style>/', '/<(\/?)b>/');
    $replace = array('<$1strong>', '', '', '<$1h2>', '', '', '<$1strong>');
    $body = preg_replace($pattern, $replace, $body);
    // Point img tags at the local copy of imported inline images.
    if (preg_match_all('/src="([^"]+)"/', $body, $matches)) {
      foreach($matches[1] as $src) {
        $full_url = (substr($src,0,1) == '/' ? "http://www.vermontlaw.edu$src" : $src);
        if (isset($this->inline_images[$full_url])) {
          $replace = $this->inline_images[$full_url]['url'] .'" data-entity-type="file" data-entity-uuid="'. $this->inline_images[$full_url]['uuid'];
          $body = str_replace($src, $replace, $body);
        }
      }
    }
    $row->setSourceProperty('content', $body);

    // Check for tags that require the full_html filter instead of basic_html.
    $filter = 'basic_html';
    foreach (array('<hr', '<sup>', '<table') as $t) {
      if (strpos($body, $t)) $filter = 'full_html';
    }
    $row->setSourceProperty('filter', $filter);

    // Create a redirect from the legacy URL.
    $path = $row->getSourceProperty('path');
    Redirect::create([
      'redirect_source' => substr($url,26),
      'redirect_redirect' => "internal:$path",
      'language' => 'und',
      'status_code' => '301',
    ])->save();

    parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    $fields['description'] = $this->t('meta description scraped from page');
    $fields['filter'] = $this->t('text filter to use');
    $fields['keywords'] = $this->t('meta keywords scraped from page');
    $fields['section'] = $this->t('section name scraped from page');
    $fields['sidebar_links'] = $this->t('sidebar links scraped from page');
    $fields['title'] = $this->t('title scraped from page');
    $fields['title_image'] = $this->t('title image scraped from page');
    return $fields;
  }
}
