<?php

namespace Drupal\vtlaw_common\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Filters by given list of nodes by no aliases.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("vtlaw_no_aliases")
 */
class NoAliases extends InOperator {

  /**
   * Private database object.
   */
  private $db;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $db) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->db = $db;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $db = $container->get('database');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $db
    );
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * Adding a condition where a node id from node table doesn't have an entry
   * in the url_alias table.
   */
  public function query() {
    $db = $this->db;

    $data = $db->select('url_alias','u')->fields('u', array('source'))->execute();

    $results = $data->fetchAll();

    $nids = [];
    foreach($results as $result) {
      $nids[] = (substr($result->source, 6));
    }
    parent::query();
    $this->query->addWhere($this->options['group'], "$this->tableAlias.$this->realField", $nids, 'NOT IN');
  }

}
