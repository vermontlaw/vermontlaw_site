<?php

/**
 * @file
 * Contains Drupal\vtlaw_common\Access\VtlawPublishAccessCheck
 */

namespace Drupal\vtlaw_common\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\publishcontent\Access\PublishContentAccess;

class VtlawPublishAccessCheck implements AccessInterface {

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, NodeInterface $node) {
    $site_config_front_path = \Drupal::config('system.site')->get('page.front');

    if ($site_config_front_path === '/node/' . $node->id()) {
      $front_page_access = $account->hasPermission('edit front page');
      return AccessResult::allowedIf($front_page_access);
    }

    // Return the original access callback results from
    // publishcontent/src/Access/PublishContentAccess.php by default.
    $publishcontent_access = new PublishContentAccess;
    $publishcontent_access_result = $publishcontent_access->access($account, $node);
    return $publishcontent_access->access($account, $node);
  }

}
