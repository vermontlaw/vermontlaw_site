<?php
namespace Drupal\vtlaw_common\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Alter the requirements for the publishcontent toggle route.
    if ($route = $collection->get('entity.node.publish')) {
      // Add an additional access check for the publish/unpublish toggle link.
      $route->setRequirement('_vtlaw_publish_access_check', 'TRUE');
    }

    // Alter access for /admin/structure menu
    if ($route = $collection->get('system.admin_structure')) {
      // Only users who have permission to edit taxonomy terms can access the menu.
      $route->setRequirement('_permission', 'administer taxonomy');
    }

    // Alter access for /admin/config menu
    if ($route = $collection->get('system.admin_config')) {
      // Only users who have permission to make/edit redirects can access the menu.
      $route->setRequirement('_permission', 'administer redirects');
    }
  }

}
