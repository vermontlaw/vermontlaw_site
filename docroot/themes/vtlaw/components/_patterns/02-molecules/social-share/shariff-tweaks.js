(function ($, Drupal) {

  Drupal.behaviors.shariffTweaks = {
    attach: function (context) {
      setTimeout(function() {
        // This is basically just for IE11; the shariff library doesn't handle adding classes well - it only adds the first class. So instead of `.fa.fa-twitter` we have `.fa`. This is basically a stop-gap until that gets fixed.
        $('.shariff', context).once('shariff-tweaks').each(function () {
          var $block = $(this);
          var $x = $block.find('.shariff-button.twitter');
          // if we find the above class, then we're good, if not, then we have the problem from above.
          if ($x.length === 0) {
            $('.theme-colored', $block).addClass('orientation-horizontal');
            var sets = [
              {
                label: 'Share on Twitter',
                iconClass: 'fa-twitter',
                buttonClass: 'twitter'
              }, {
                label: 'Share on Facebook',
                iconClass: 'fa-facebook',
                buttonClass: 'facebook'
              }, {
                label: 'Share on LinkedIn',
                iconClass: 'fa-linkedin',
                buttonClass: 'linkedin'
              }, {
                label: 'Pin it on Pinterest',
                iconClass: 'fa-pinterest',
                buttonClass: 'pinterest'
              }, {
                label: 'Send by email',
                iconClass: 'fa-envelope',
                buttonClass: 'mail'
              }
            ];
            sets.forEach(function (set) {
              var $link = $('a[aria-label="' + set.label + '"]', $block);
              $('.fa', $link).addClass(set.iconClass);
              $link.parent().addClass(set.buttonClass);
            });
          }
        });
      }, 1000);
    }
  };

}(jQuery, Drupal));
