## Person Listings
We have a few different versions of this block, each with different data displayed.

`_person.twig` is a base template. To use a person molecule, include either `person-short.twig` or `person-detailed.twig` and be sure to provide all the data in the corresponding `.yml` files.