(function ($) {

  // Remembers if users had menu open or closed and persists choice across other pages, unless it's the front page or the mobile version of the site.
  function menuMemory() {
    var isFront = $('.page').hasClass('page--front');
    var isMobile = $('body').width() < 590;
    if (!isMobile && !isFront) {
      var menuToggle = document.getElementById('left-column-toggle');

      function isMenuOpen() {
        return !menuToggle.checked;
      }

      function get() {
        return JSON.parse(localStorage.getItem('menuOpen'));
      }

      function set(value) {
        localStorage.setItem('menuOpen', JSON.stringify(value));
      }

      if (get()) {
        menuToggle.checked = false;
      } else {
        menuToggle.checked = true;
      }

      $('.left-column-toggle__label').click(function () {
        // need to wait the briefest moment to get real value
        setTimeout(function () {
          set(isMenuOpen());
        }, 0);
      });
    }
  }

  $(document).ready(function () {
    menuMemory();
  });
})(jQuery);
