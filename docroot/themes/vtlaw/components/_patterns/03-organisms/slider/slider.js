(function($) {
$(document).ready(function(){
  $('.slider--standard').slick({
    dots: true,
    autoplay: true
  });

  $('.slider--home').slick({
    dots: false,
    autoplaySpeed: 5000,
    autoplay: true
  });

  // move dots and arrows to caption box - init
  $.each($('.slider--standard'), function() {
    var activeCaption = $(this).find('.slick-active .slide__caption_content');
    activeCaption.append($(this).find(".slick-dots"));
    activeCaption.append($(this).find(".slick-prev"));
    activeCaption.append($(this).find(".slick-next"));
  });

  // move dots and arrows to caption box - listener
  $('.slider--standard').on('afterChange', function(event, slick, currentSlide, nextSlide){
    var activeCaption = $(this).find('.slick-active .slide__caption_content');
    activeCaption.append($(this).find(".slick-dots"));
    activeCaption.append($(this).find(".slick-prev"));
    activeCaption.append($(this).find(".slick-next"));
  });

  // recalculate slide width when left column is toggled
  $('.left-column-toggle__label').on('click touch', function () {
    // wait for the css transition to end
    $('.page__content').one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
              function(event) {
      $('.slider').slick('setPosition');
    });
  });

});
})(jQuery);
