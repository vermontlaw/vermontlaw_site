#!/bin/bash

# Parse args
while [[ $# > 1 ]]
do
key="$1"
case $key in
    -b|--branch)
    BRANCH="$2"
    shift
    ;;
    -h|--help)
    SHOWHELP="$2"
    shift
    ;;
    *)
    ;;
esac
shift
done

# The default branch is the latest `release/VERSION` branch.
DEFAULT_BRANCH=`git for-each-ref --count=1 --sort=-committerdate refs/heads/release/* --format='%(refname:short)'`

if [ -z ${BRANCH} ]; then
  BRANCH=${DEFAULT_BRANCH}
fi

# See if the branch exists.
git rev-parse --verify $BRANCH > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Release branch does not exist."
  exit
fi

if [ -z ${BRANCH} ] || [ ${SHOWHELP} ]; then
  echo "Usage: ./deploy-stage.sh -b <branch> -h";
  echo "-b  or --branch  branch to be deployed, optional, default value is ${BRANCH}";
  echo "-h  or --help    show this help";
  exit
fi

ssh vtlaw@18.204.247.41 BRANCH="${BRANCH}" 'bash -s' <<'ENDSSH'
source ~/.profile
nvm use 6.6.0
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval `ssh-agent -s`
fi
ssh-add ~/.ssh/id_rsa

BACKUP_DB_DIR="backups"
timestamp=$(date +%Y%m%d%H%M)
mysqldump --hex-blob --user=root --password=$SQLROOT vtlaw -r ${BACKUP_DB_DIR}/vtlaw.${timestamp}.mysql 2> ${BACKUP_DB_DIR}/dumpError.tmp

cd www

# Temporarily adjust permissions on settings file in case it changed in the repo.
chmod +w docroot/sites/default/
chmod +w docroot/sites/default/settings.php

# Pull changes from the main bitbucket repo. Since `www/docroot/` is the web
# root, it's safe to allow `www/` to be a git clone.
git reset --hard HEAD
git fetch origin -pv
git checkout ${BRANCH}
git pull origin

echo UPDATING DRUPAL PACKAGES...
echo
composer install --no-dev
composer drupal-scaffold

// Restore changes to scaffold files like .htaccess.
git checkout -- docroot/.htaccess

# Create a settings.local.php file with database credentials.
if [ ! -f docroot/sites/default/settings.local.php ]; then
  touch docroot/sites/default/settings.local.php
  cat << EOF >> docroot/sites/default/settings.local.php
<?php

\$databases['default']['default'] = array (
  'database' => 'vtlaw',
  'username' => 'vtlaw',
  'password' => '$SQLPASSWD',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
EOF
fi

# Revert permissions on settings files.
chmod -w docroot/sites/default/
chmod -w docroot/sites/default/settings.php

# Drush commands (run in the docroot/ directory).
cd docroot/

echo UPDATING DRUPAL...
echo
../vendor/bin/drush cr && ../vendor/bin/drush updb -y && ../vendor/bin/drush cim -y && ../vendor/bin/drush cr &

# We are now going to push compiled assets with our deployments, not compile them on the server.
echo DONE!
echo
ENDSSH
