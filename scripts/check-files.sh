#!/bin/bash

while true; do
    read -p "Are you connected to VPN? " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

ssh vtlaw@11.0.2.90 TAG="${TAG}" 'bash -s' <<'ENDSSH'
cd ~/www; md5sum -c ~/checklist.chk | while read -r line; do [[ "$line" =~ *"phpunit.xml.dist"* ]] || [[ "$line" =~ *"migration_data"* ]] || [[ "$line" =~ *"docroot/sites/default/files"* ]] || [[ "$line" =~ "OK" ]] || echo "md5sum failed for file: $line"; done
exit
ENDSSH