#!/bin/bash

# Parse args
while [[ $# > 1 ]]
do
key="$1"
case $key in
    -r|--repository)
    REPOSITORY="$2"
    shift
    ;;
    -b|--branch)
    BRANCH="$2"
    shift
    ;;
    -s|--server)
    SERVER="$2"
    shift
    ;;
    -u|--user)
    SERVERUSER="$2"
    shift
    ;;
    -f|--folder)
    SERVERFOLDER="$2"
    shift
    ;;
    -h|--help)
    SHOWHELP="$2"
    shift
    ;;
    *)
    ;;
esac
shift
done

# Setup default args
DEFAULT_REPOSITORY="ssh://git@bitbucket.org/colabcoop-ondemand/vtlaw.git"
DEFAULT_BRANCH="develop"
DEFAULT_SERVER="34.201.18.255"
DEFAULT_SERVERUSER="vtlaw"
DEFAULT_SERVERFOLDER="/opt/www/vtlaw/www"

# Check for provided vs default args
if [ -z ${REPOSITORY} ]; then
  REPOSITORY=${DEFAULT_REPOSITORY}
fi

if [ -z ${BRANCH} ]; then
  BRANCH=${DEFAULT_BRANCH}
fi

if [ -z ${SERVER} ]; then
  SERVER=${DEFAULT_SERVER}
fi

if [ -z ${SERVERUSER} ]; then
  SERVERUSER=${DEFAULT_SERVERUSER}
fi

if [ -z ${SERVERFOLDER} ]; then
  SERVERFOLDER=${DEFAULT_SERVERFOLDER}
fi

# Print help
if [ -z ${REPOSITORY} ] || [ -z ${BRANCH} ] || [ -z ${SERVER} ] || [ -z ${SERVERUSER} ] || [ -z ${SERVERFOLDER} ] || [ ${SHOWHELP} ]; then
  echo "Usage: ./deploy-dev.sh -r <repository> -b <branch> -s <server> -u <user> -f <folder> -h";
  echo "-r  or --repository     repository to be deployed, optional, default value is ${DEFAULT_REPOSITORY}";
  echo "-b  or --branch         branch to be deployed, optional, default value is ${DEFAULT_BRANCH}";
  echo "-s  or --server         server to be deployed, optional, default value is ${DEFAULT_SERVER}";
  echo "-u  or --user           user to be deployed, optional, default value is ${DEFAULT_USER}";
  echo "-f  or --folder         folder to be deployed, optional, default value is ${DEFAULT_FOLDER}";
  echo "-h  or --help           show this help";
  exit
fi

# Setup slugs
SLUG=$(echo "$BRANCH" | awk '{print tolower($0)}')
DBSLUG=vtlaw__$(echo "$SLUG" | tr "-" "_")

# Print all vars
echo "REPOSITORY: ${REPOSITORY}"
echo "BRANCH: ${BRANCH}"
echo "SLUG: ${SLUG}"
echo "DBSLUG: ${DBSLUG}"
echo "SERVER: ${SERVER}"
echo "SERVERUSER: ${SERVERUSER}"
echo "SERVERFOLDER: ${SERVERFOLDER}"
echo

# Perform deployment
ssh ${SERVERUSER}@${SERVER} REPOSITORY="${REPOSITORY}" BRANCH="${BRANCH}" SLUG="${SLUG}" DBSLUG="${DBSLUG}" SERVERFOLDER="${SERVERFOLDER}" "bash -s" << "ENDSSH"
# Start SSH agent, add identity and init nvm
source ~/.profile
nvm use 6.6.0
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval `ssh-agent -s`
fi
ssh-add ~/.ssh/id_rsa

# Create database and grant access to it if it doesn't exist.
OUTPUT=`mysqlshow --user="root" --password="${SQLROOT}" "${DBSLUG}" | grep -v Wildcard | grep -o ${DBSLUG}`
if [ "$OUTPUT" != "$DBSLUG" ]; then
    mysql --user="root" --password="${SQLROOT}" -e "create database ${DBSLUG}"
    mysql --user="root" --password="${SQLROOT}" -e "grant all privileges on ${DBSLUG}.* to vtlaw@localhost identified by '${SQLPASSWD}'"
fi

# Create the web directory if it doesn't exist.
cd ${SERVERFOLDER}
mkdir -p ${SLUG}

# Clone or update the repository/branch:
if [ "$OUTPUT" != "$DBSLUG" ]; then
    echo CLONING CODE...
    echo
    git clone -b ${BRANCH} ${REPOSITORY} ${SLUG}
fi

echo UPDATING CODE...
cd ${SLUG}
chmod -R +w docroot/sites/default/

git reset --hard HEAD
git fetch --all
git checkout ${BRANCH}
git pull origin ${BRANCH}
git checkout -- docroot/.htaccess

# Update the composer packages.
echo UPDATING DRUPAL PACKAGES...
echo
composer install &&
composer drupal-scaffold &&

# Create a settings.local.php file with database credentials.
if [ ! -f docroot/sites/default/settings.local.php ]; then
  touch docroot/sites/default/settings.local.php
  cat << EOF >> docroot/sites/default/settings.local.php
<?php

\$databases['default']['default'] = array (
  'database' => '$DBSLUG',
  'username' => 'vtlaw',
  'password' => '$SQLPASSWD',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
EOF
fi

# Revert permissions on settings files.
chmod -w docroot/sites/default/
chmod -w docroot/sites/default/settings.php

# Tests
#[RUN_REMOTE_TESTS_COMMAND] &&

# Drush commands (run in the docroot/ directory).
cd docroot/

# Run drush commands for either new or existing branch build:
if [ "$OUTPUT" != "$DBSLUG" ]; then
    echo INSTALLING DRUPAL...
    echo
    ../vendor/bin/drush si --config-dir=../config/sync -y &
else
    echo UPDATING DRUPAL...
    echo
    ../vendor/bin/drush cr && ../vendor/bin/drush updb -y && ../vendor/bin/drush cim -y && ../vendor/bin/drush cr &
fi

# We are now going to push compiled assets with our deployments, not compile them on the server.
echo DONE!
echo
ENDSSH
