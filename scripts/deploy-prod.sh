#!/bin/bash

while true; do
    read -p "Are you connected to VPN? " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

# Parse args
while [[ $# > 1 ]]
do
key="$1"
case $key in
    -t|--tag)
    TAG="$2"
    shift
    ;;
    -h|--help)
    SHOWHELP="$2"
    shift
    ;;
    *)
    ;;
esac
shift
done

DEFAULT_TAG="latest"

if [ -z ${TAG} ]; then
  TAG=${DEFAULT_TAG}
fi

# See if the tag exists.
git rev-parse "$TAG^{tag}" >/dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Release tag does not exist."
  exit
fi

if [ -z ${TAG} ] || [ ${SHOWHELP} ]; then
  echo "Usage: ./deploy-prod.sh -t <tag> -h";
  echo "-t  or --tag     tag to be deployed, optional, default value is ${TAG}";
  echo "-h  or --help    show this help";
  exit
fi

ssh vtlaw@11.0.2.90 TAG="${TAG}" 'bash -s' <<'ENDSSH'
source ~/.profile
nvm use 6.6.0
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval `ssh-agent -s`
fi
ssh-add ~/.ssh/id_rsa

# @todo Consider naming backups after the tag to be deployed (e.g. `vtlaw.pre-${TAG}.mysql`). This could be useful for automated restores.
BACKUP_DB_DIR="backups"
if [ ! -d ${BACKUP_DB_DIR} ]; then
    mkdir -p ${BACKUP_DB_DIR}
fi
mysqldump --hex-blob --user=vtlawadmin --password=$SQLROOT -h vtlaw-db.ce6vkgevwhup.us-east-1.rds.amazonaws.com vtlaw -r ${BACKUP_DB_DIR}/vtlaw.latest.mysql 2> ${BACKUP_DB_DIR}/dump_error.log

cd ~/www

# Temporarily adjust permissions on settings file in case it changed in the repo.
chmod +w docroot/sites/default/
chmod +w docroot/sites/default/settings.php

# Pull changes from the main bitbucket repo. Since `www/docroot/` is the web
# root, it's safe to allow `www/` to be a git clone.
git reset --hard HEAD
git fetch --tags -pv
git checkout ${TAG}

echo UPDATING DRUPAL PACKAGES...
echo
composer install --no-dev
composer drupal-scaffold

// Restore changes to scaffold files like .htaccess.
git checkout -- docroot/.htaccess

# Create a settings.local.php file with database credentials.
if [ ! -f docroot/sites/default/settings.local.php ]; then
  touch docroot/sites/default/settings.local.php
  cat << EOF >> docroot/sites/default/settings.local.php
<?php

$settings['reverse_proxy'] = TRUE;
$settings['reverse_proxy_addresses'] = array('11.0.1.40');
$settings['reverse_proxy_header'] = 'X_CLUSTER_CLIENT_IP';
$settings['reverse_proxy_proto_header'] = 'X_FORWARDED_PROTO';
$settings['reverse_proxy_host_header'] = 'X_FORWARDED_HOST';
$settings['reverse_proxy_port_header'] = 'X_FORWARDED_PORT';
$settings['reverse_proxy_forwarded_header'] = 'FORWARDED';

\$databases['default']['default'] = array (
  'database' => 'vtlaw',
  'username' => 'vtlaw',
  'password' => '$SQLPASSWD',
  'prefix' => '',
  'host' => 'vtlaw-db.ce6vkgevwhup.us-east-1.rds.amazonaws.com',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
EOF
fi

# Revert permissions on settings files.
chmod -w docroot/sites/default/
chmod -w docroot/sites/default/settings.php

# Drush commands (run in the docroot/ directory).
cd docroot/

echo UPDATING DRUPAL...
echo
../vendor/bin/drush cr && ../vendor/bin/drush updb -y && ../vendor/bin/drush cim -y && ../vendor/bin/drush cr &

# We are now going to push compiled assets with our deployments, not compile them on the server.

cd ~/www

# Create md5 file hashes checklist
find -type f -exec md5sum "{}" + > ~/checklist.chk

# Send PURGE request to Varnish instance
curl -X "PURGE" 11.0.1.40

echo
echo DONE!
echo

exit
ENDSSH
