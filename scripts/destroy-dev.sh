#!/bin/bash

# Parse args
while [[ $# > 1 ]]
do
key="$1"
case $key in
    -b|--branch)
    BRANCH="$2"
    shift
    ;;
    -s|--server)
    SERVER="$2"
    shift
    ;;
    -u|--user)
    SERVERUSER="$2"
    shift
    ;;
    -f|--folder)
    SERVERFOLDER="$2"
    shift
    ;;
    -h|--help)
    SHOWHELP="$2"
    shift
    ;;
    *)
    ;;
esac
shift
done

# Setup default args
DEFAULT_BRANCH="develop"
DEFAULT_SERVER="34.201.18.255"
DEFAULT_SERVERUSER="vtlaw"
DEFAULT_SERVERFOLDER="/opt/www/vtlaw/www"

# Check for provided vs default args
if [ -z ${BRANCH} ]; then
  BRANCH=${DEFAULT_BRANCH}
fi

if [ -z ${SERVER} ]; then
  SERVER=${DEFAULT_SERVER}
fi

if [ -z ${SERVERUSER} ]; then
  SERVERUSER=${DEFAULT_SERVERUSER}
fi

if [ -z ${SERVERFOLDER} ]; then
  SERVERFOLDER=${DEFAULT_SERVERFOLDER}
fi

# Print help
if [ -z ${BRANCH} ] || [ -z ${SERVER} ] || [ -z ${SERVERUSER} ] || [ -z ${SERVERFOLDER} ] || [ ${SHOWHELP} ]; then 
  echo "Usage: ./deploy-dev.sh -b <branch> -s <server> -u <user> -f <folder> -h"; 
  echo "-b  or --branch         branch to be destroyed, optional, default value is ${DEFAULT_BRANCH}";
  echo "-s  or --server         server to be destroyed, optional, default value is ${DEFAULT_SERVER}";
  echo "-u  or --user           user to be destroyed, optional, default value is ${DEFAULT_USER}";
  echo "-f  or --folder         folder to be destroyed, optional, default value is ${DEFAULT_FOLDER}";
  echo "-h  or --help           show this help";
  exit
fi

# Setup slugs
SLUG=$(echo "$BRANCH" | awk '{print tolower($0)}')
DBSLUG=vtlaw__$(echo "$SLUG" | tr "-" "_")

# Print all vars
echo "BRANCH: ${BRANCH}"
echo "SLUG: ${SLUG}"
echo "DBSLUG: ${DBSLUG}"
echo "SERVER: ${SERVER}"
echo "SERVERUSER: ${SERVERUSER}"
echo "SERVERFOLDER: ${SERVERFOLDER}"

# Perform deployment
ssh ${SERVERUSER}@${SERVER} BRANCH="${BRANCH}" SLUG="${SLUG}" DBSLUG="${DBSLUG}" SERVERFOLDER="${SERVERFOLDER}" "bash -s" << "ENDSSH"
# Destroy database
source ~/.profile
mysql --user="root" --password="${SQLROOT}" -e "drop database ${DBSLUG}"

# Destroy files
cd ${SERVERFOLDER}
rm -rf {SLUG}
ENDSSH