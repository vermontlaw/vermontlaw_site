#!/bin/bash

while true; do
    read -p "Are you connected to VPN? " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

ssh -t -t -A vtlaw@11.0.2.90 'bash -s' <<'ENDSSH'
source ~/.profile
mysqldump --user=vtlawadmin --password=$SQLROOT -h vtlaw-db.ce6vkgevwhup.us-east-1.rds.amazonaws.com vtlaw > db.sql &&
scp -r ~/db.sql vtlaw@54.146.187.90:/opt/www/vtlaw/db.sql
exit
ENDSSH

ssh -t -t -A vtlaw@54.146.187.90 'bash -s' <<'ENDSSH'
source ~/.profile
if [ ! -f db.sql ]; then
    echo "No dump file to import"
    exit;
fi
mysql --user=root --password=$SQLROOT -h localhost -e "SHOW DATABASES;" &&
mysql --user=root --password=$SQLROOT -h localhost -e "DROP DATABASE vtlaw;" &&
mysql --user=root --password=$SQLROOT -h localhost -e "CREATE DATABASE vtlaw;" &&
mysql --user=root --password=$SQLROOT -h localhost -e "GRANT USAGE ON vtlaw.* TO 'vtlaw'@'localhost' IDENTIFIED BY '"$SQLPASSWD"' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;" &&
mysql --user=root --password=$SQLROOT -h localhost -e "GRANT ALL PRIVILEGES ON vtlaw.* TO 'vtlaw'@'localhost' IDENTIFIED BY '"$SQLPASSWD"';" &&
mysql --user=root --password=$SQLROOT -h localhost vtlaw < db.sql
exit
ENDSSH

rm -rf ~/db.sql