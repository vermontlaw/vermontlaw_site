#!/bin/bash

DEFAULT_LOCAL_HOSTNAME=`git config drush.uri`
[ -z "${DEFAULT_LOCAL_HOSTNAME}" ] && DEFAULT_LOCAL_HOSTNAME='vtlaw.dev'

read -p "Enter your local development hostname [${DEFAULT_LOCAL_HOSTNAME}]: " LOCAL_HOSTNAME
[ -z "${LOCAL_HOSTNAME}" ] && LOCAL_HOSTNAME=$DEFAULT_LOCAL_HOSTNAME

# Set up post-push aka deploy hook
git config alias.deploy '!./scripts/deploy-dev.sh -b $(if [ -z $2 ]; then echo $(git symbolic-ref --short HEAD); else echo $2; fi)'

# Set up static stage and prod deploy hooks
git config alias.deploy-prod '!./scripts/deploy-prod.sh -t $1'
git config alias.deploy-stage '!./scripts/deploy-stage.sh -b $1'

# Set up util hooks
git config alias.check-files '!./scripts/check-files.sh'
git config alias.migrate-db '!./scripts/migrate-db.sh'

# Set up destroy hook
git config alias.destroy '!if [ -z $1 ]; then echo "Specify branch to destroy"; else ./scripts/destroy-dev.sh -b $1; fi'

# Set up local drush alias settings. See drush/vls.aliases.drushrc.php.
git config drush.root `git rev-parse --show-toplevel`/docroot
git config drush.uri $LOCAL_HOSTNAME # ...or whatever your local hostname is.

# Set up pre-push aka local tests hook
# tee '[RUN_LOCAL_TESTS_COMMAND]' >> ../.git/hooks/pre-push
